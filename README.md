# Task Force FullStack Challenge (Java/python)

Create a full web application using the following stacks,
Back-end: Use any Java or Python framework/library you are comfortable with.
Front-end: Use any front-end framework/library you are most comfortable with.

### 🔖Requirements

**A use case:** Elie is an employer of Awesomity Lab, and as a person, he has expenses and income, 
but it is really hard for him to manage all those in and out transactions because he uses different accounts while doing all those transactions, 
some of those accounts are; bank accounts, mobile money account, cash, etc.

As a developer, Elie approaches you to develop a web application for him that has the following features;
   - The UI should follow the same specs, color theme and assets as shown on the XD link below
   - Mobile flexibility should be taken into account.
   - Track all in and out transactions from each account.
   - Provide a report according to the desired time gap.
   - Set a budget to not exceed on a monthly basis, once exceed the application should notify him via email/message.
   - To be able to add categories.
   - To be able to link his expenses their related categories.
   - Display transactions summary in a visualized way.
   
>Your project should be on an online repository (Gitlab, Github,...)


### 📝Submission
- XD link Here https://xd.adobe.com/view/6cf6f179-0d96-4689-81c2-56e328ed4d09-39a5/
- Submit via this Google form https://forms.gle/LhXfBemhKDQjvRmG7.
- Deadline: 31 December 2021 at noon, submit before then.

### 👷🏽‍♀️Best Practices

- Document your API eg: [Swagger](https://swagger.io/), Postman
- Apply validation (phone number must be a Rwandan number, and email should be validated).
- The system should throw an exception if any error occurs.
- Properly log your application.
- Do not hard code any sensitive data (eg: env variables).
- Write a good README file with the steps to Install, Test & run your application (steps for docker included)

### ✨Bonus

- System Logs
   - The system should record all the user activities (login, logout, password reset, profile update etc…) in order to comply with external audit requirements.
- Testing
   - Integration tests for all your use cases.
   - Write unit tests with a minimum of 60% coverage.
- Have a Dockerized environment(`Dockerfile`, `docker-compose.yml`)
- Allow the user to export his transactions in a pdf/excel format.
- Allow the user to login using social logins (google,facebook,etc...)
